P=$(shell pwd)

.PHONY: build clean rebuild

./lib.o: ./lib.asm
	nasm -f elf64 -g -o ./lib.o $(P)/lib.asm

./lib.inc.o: ./lib.inc
	nasm -f elf64 -g -o ./lib.inc.o $(P)/lib.inc

./main.o: ./main.asm

%.o: %.asm
	nasm -f elf64 -o $@ $(P)/$<

build: main.o lib.o lib.inc dict.o dict.inc words.inc colon.inc
	ld -o ./executable main.o lib.o dict.o

test:
	python3 tests.py

clean:
	rm -f ./main.o
	rm -f ./executable

rebuild: clean build
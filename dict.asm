%include "lib.inc"

section .text
global find_word

find_word:
	push r12
	
	.loop: 
		mov r12, rsi
		lea rsi, [rsi + 8]
		call string_equals
		
		test rax, rax; �����
		jnz .success

		mov rsi, qword[r12]

		test rsi, rsi
		jz .end_of_list


		jmp .loop

	.success:
		mov rax, r12
		jmp .return_block

	.end_of_list:
		mov rax, 0

	.return_block:
		pop r12
		ret
%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define MAX_LENGTH 255
%define BSS_SIZE 256

section .rodata
	max_length_error: db 'BFOverflow', 0
	not_found_error: db 'Key is not here!', 0

section .bss
	bf: resb BSS_SIZE

section .text
	global _start

_start:
	mov rdi, bf
	mov rsi, MAX_LENGTH
	call read_word

	test rax, rax
	jz .max_length_exceeded

	mov rdi, rax
	mov rsi, first_word
	call find_word

	test rax, rax
	jz .list_have_no_elem

	lea rdi, [rax+8]
	push rdi
	call string_length
	pop r13
	add rax, r13
	inc rax
	mov rdi, rax
	call print_string
	jmp .exit

.list_have_no_elem:
	mov rdi, not_found_error
    call print_string

.exit:
	call exit

.max_length_exceeded:
	mov rdi, max_length_error
	call print_string
	jmp .exit




    

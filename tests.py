import unittest
import subprocess


class Tests(unittest.TestCase):
    def start_application(self, stdin_input):
        proc = subprocess.Popen(
            ["./executable"],
            text=True,
            shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = proc.communicate(
            input=stdin_input
        )
        return stdout.strip(), stderr.strip()

    def test_key_is_here(self):
        self.assertEqual(
            self.start_application("first_slovo"),
            ("slovo_1", ""),
        )
        self.assertEqual(
            self.start_application("second_slovo"),
            ("slovo_2", ""),
        )

    def test_key_is_not_here(self):
        self.assertEqual(
            self.start_application("a"),
            ("Key is not here!", ""),
        )

    def test_buffer_overflow(self):
        self.assertEqual(
            self.start_application("_" * 257),
            ("BFOverflow", ""),
        )


if __name__ == "__main__":
    unittest.main()

